FROM java:8-jre
MAINTAINER Xawirses <xawirses@gmail.com>
LABEL maintainer="Xawirses <xawirses@gmail.com>"

RUN apt update && apt install -y unzip

ENV MINECRAFT_VERSION 1.4.7
ENV FTB_ULTIMATE_VERSION 1.1.2
ENV FTB_ULTIMATE_FILE_NUM 2260291
ENV FTB_ULTIMATE_JAR ftbserver.jar
ENV FTB_ULTIMATE_NAME Ultimate-${FTB_ULTIMATE_VERSION}-${MINECRAFT_VERSION}Server
ENV FTB_ULTIMATE_ZIP ${FTB_ULTIMATE_NAME}.zip
ENV FTB_ULTIMATE_URL https://www.feed-the-beast.com/projects/ftb-ultimate/files/${FTB_ULTIMATE_FILE_NUM}/download

RUN mkdir /minecraft
WORKDIR /minecraft

RUN curl --create-dirs -sLo /minecraft/${FTB_ULTIMATE_ZIP} ${FTB_ULTIMATE_URL} && \
	unzip ${FTB_ULTIMATE_ZIP}

COPY entrypoint.sh /usr/bin/entrypoint.sh
RUN chmod +x /usr/bin/entrypoint.sh

ENV JAVA_RAM 4G
ENV JAVA_OPTS -XX:+UseG1GC -Dsun.rmi.dgc.server.gcInterval=2147483646 -XX:+UnlockExperimentalVMOptions -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=32M

EXPOSE 25565

ENTRYPOINT ["/usr/bin/entrypoint.sh"]